package nl.comunit.spidermobile.plugin.imei;

import android.content.Context;
import android.telephony.TelephonyManager;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;

/**
 *
 * @author Mihai Pistol
 */
public class IMEIRetriver extends CordovaPlugin {

    private String IMEI;

    @Override
    public void initialize (CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        Logger.getLogger("IMEIRetriver").log(Level.INFO, "initialized()");
        Context context = cordova.getActivity().getApplicationContext();
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        IMEI = telephonyManager.getDeviceId();
    }

    @Override
    public boolean execute (String action, JSONArray data, CallbackContext callback) throws JSONException {
        callback.success(IMEI);
        return true;
    }
}
