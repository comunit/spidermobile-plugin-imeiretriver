/*jslint browser: true, continue: true, newcap: true, plusplus: true, unparam: true, todo: true, white: true */

/**
 * @author Mihai Pistol
 */
(function () {
    "use strict";

    function IMEIRetriver () {
    }

    /**
     * @function
     * @public
     * @memberOf {IMEIRetriver}
     * @param {Function} successHandler
     * @param {Function} failureHandler
     * @returns {undefined}
     */
    IMEIRetriver.prototype.getImei = function (successHandler, failureHandler) {
        window.cordova.exec(successHandler, failureHandler, "IMEIRetriver", "", []);
    };

    IMEIRetriver.install = function () {
        if (!window.plugins) {
            window.plugins = {};
        }
        window.plugins.IMEIRetriver = new IMEIRetriver();
        return window.plugins.IMEIRetriver;
    };

    window.cordova.addConstructor(IMEIRetriver.install);
}());
